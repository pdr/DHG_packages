Source: haskell-mueval
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Ilias Tsitsimpis <iliastsi@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 haskell-devscripts (>= 0.13),
 ghc (>= 9.4),
 ghc-prof,
 libghc-quickcheck2-dev (>= 2.14.3),
 libghc-quickcheck2-dev (<< 2.16),
 libghc-quickcheck2-prof,
 libghc-extensible-exceptions-dev (>= 0.1.1.4),
 libghc-extensible-exceptions-dev (<< 0.2),
 libghc-extensible-exceptions-prof,
 libghc-hint-dev (>= 0.9.0.7),
 libghc-hint-dev (<< 0.10),
 libghc-hint-prof,
 libghc-show-dev (>= 0.6),
 libghc-show-dev (<< 0.7),
 libghc-show-prof,
 libghc-simple-reflect-dev (>= 0.3.3),
 libghc-simple-reflect-dev (<< 0.4),
 libghc-simple-reflect-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-extensible-exceptions-doc,
 libghc-hint-doc,
 libghc-quickcheck2-doc,
 libghc-show-doc,
 libghc-simple-reflect-doc,
Standards-Version: 4.7.0
Homepage: https://github.com/TerenceNg03/mueval#readme
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-mueval
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-mueval]
X-Description: safely evaluate pure Haskell expressions
 Mueval is a Haskell interpreter. It uses the GHC API to evaluate arbitrary
 Haskell expressions.  Importantly, mueval takes many precautions to defang and
 avoid "evil" code.  It uses resource limits, whitelisted modules and Safe
 Haskell, special Show instances for IO, threads, processes, and changes of
 directory to sandbox the Haskell code.
 .
 It is, in short, intended to be a standalone version of Lambdabot's famous
 evaluation functionality.

Package: mueval
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Safely evaluate pure Haskell expressions - executables
 Mueval is a Haskell interpreter. It uses the GHC API to evaluate arbitrary
 Haskell expressions.  Importantly, mueval takes many precautions to defang and
 avoid "evil" code.  It uses resource limits, whitelisted modules and Safe
 Haskell, special Show instances for IO, threads, processes, and changes of
 directory to sandbox the Haskell code.
 .
 This contains the stand-alone mueval binaries.

Package: libghc-mueval-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-mueval-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-mueval-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
