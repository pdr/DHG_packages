Source: haskell-ogma-cli
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Scott Talbert <swt@techie.net>
Priority: optional
Section: non-free/haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.13),
 cdbs,
 ghc,
 ghc-prof,
 libghc-hunit-dev (>= 1.2.0.0),
 libghc-hunit-dev (<< 1.7),
 libghc-hunit-prof,
 libghc-ogma-core-dev (>= 1.6.0),
 libghc-ogma-core-dev (<< 1.7),
 libghc-ogma-core-prof,
 libghc-optparse-applicative-dev (>= 0.14),
 libghc-optparse-applicative-dev (<< 0.19),
 libghc-optparse-applicative-prof,
 libghc-test-framework-dev (>= 0.8.2),
 libghc-test-framework-dev (<< 0.9),
 libghc-test-framework-prof,
 libghc-test-framework-hunit-dev (>= 0.2.0),
 libghc-test-framework-hunit-dev (<< 0.4),
 libghc-test-framework-hunit-prof,
Standards-Version: 4.7.0
Homepage: https://github.com/nasa/ogma
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-ogma-cli
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-ogma-cli]
X-Description: Helper tool to interoperate between Copilot and other languages
 Ogma is a tool to facilitate the integration of safe runtime monitors into
 other systems. Ogma extends
 <https://github.com/Copilot-Language/copilot Copilot>, a high-level runtime
 verification framework that generates hard real-time C99 code.
 .
 Some use cases supported by Ogma include:
 .
  - Translating requirements defined in structured natural
  language into monitors in Copilot.
 .
  - Generating the glue code necessary to work with C
  structs in Copilot.
 .
  - Generating
  <https://cfs.gsfc.nasa.gov/ NASA Core Flight System>
  applications that use Copilot for monitoring data
  received from the message bus.
 .
  - Generating message handlers for NASA Core Flight System
  applications to make external data in structs available
  to a Copilot monitor.
 .
  - Generating
  <https://ros.org Robot Operating System (ROS 2)>
  applications that use Copilot for monitoring data
  received from different topics.
 .
  - Generating
  <https://github.com/nasa/fprime F'>
  components that use Copilot for monitoring.
 .
  - Generating monitors from state diagrams specified using
  a graphical notation.

Package: haskell-ogma-cli-utils
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
