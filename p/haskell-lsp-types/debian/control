Source: haskell-lsp-types
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Pete Ryland <pdr@pdr.cx>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 haskell-devscripts (>= 0.13),
 cdbs,
 debhelper-compat (= 10),
 ghc,
 ghc-prof,
 libghc-diff-dev (>= 0.4),
 libghc-diff-dev (<< 0.6),
 libghc-diff-prof,
 libghc-aeson-dev (>= 2),
 libghc-aeson-dev (<< 2.3),
 libghc-aeson-prof,
 libghc-data-default-dev (>= 0.7),
 libghc-data-default-dev (<< 0.8),
 libghc-data-default-prof,
 libghc-dlist-dev (>= 1.0),
 libghc-dlist-dev (<< 1.1),
 libghc-dlist-prof,
 libghc-file-embed-dev (>= 0.0.15),
 libghc-file-embed-dev (<< 1.0),
 libghc-file-embed-prof,
 libghc-generic-arbitrary-dev,
 libghc-generic-arbitrary-prof,
 libghc-hashable-dev (>= 1.4),
 libghc-hashable-dev (<< 1.5),
 libghc-hashable-prof,
 libghc-indexed-traversable-dev (>= 0.1),
 libghc-indexed-traversable-dev (<< 0.2),
 libghc-indexed-traversable-prof,
 libghc-indexed-traversable-instances-dev (>= 0.1),
 libghc-indexed-traversable-instances-dev (<< 0.2),
 libghc-indexed-traversable-instances-prof,
 libghc-lens-dev (>= 5.1),
 libghc-lens-dev (<< 5.3),
 libghc-lens-prof,
 libghc-lens-aeson-dev (>= 1.2),
 libghc-lens-aeson-dev (<< 1.3),
 libghc-lens-aeson-prof,
 libghc-mod-dev (>= 0.2),
 libghc-mod-dev (<< 0.3),
 libghc-mod-prof,
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-dev (<< 2.7),
 libghc-network-uri-prof,
 libghc-prettyprinter-dev (>= 1.7),
 libghc-prettyprinter-dev (<< 1.8),
 libghc-prettyprinter-prof,
 libghc-quickcheck-instances-dev,
 libghc-quickcheck-instances-prof,
 libghc-row-types-dev (>= 1.0),
 libghc-row-types-dev (<< 1.1),
 libghc-row-types-prof,
 libghc-safe-dev (>= 0.3),
 libghc-safe-dev (<< 0.4),
 libghc-safe-prof,
 libghc-some-dev (>= 1.0),
 libghc-some-dev (<< 1.1),
 libghc-some-prof,
 libghc-prettyprinter-dev,
 libghc-regex-dev,
 libghc-regex-prof,
Build-Depends-Indep: ghc-doc,
 libghc-diff-doc,
 libghc-aeson-doc,
 libghc-data-default-doc,
 libghc-dlist-doc,
 libghc-file-embed-doc,
 libghc-generic-arbitrary-doc,
 libghc-hashable-doc,
 libghc-indexed-traversable-doc,
 libghc-indexed-traversable-instances-doc,
 libghc-lens-doc,
 libghc-lens-aeson-doc,
 libghc-mod-doc,
 libghc-network-uri-doc,
 libghc-prettyprinter-doc,
 libghc-quickcheck-instances-doc,
 libghc-row-types-doc,
 libghc-safe-doc,
 libghc-some-doc,
Standards-Version: 4.7.0
Homepage: https://github.com/haskell/lsp
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-lsp-types
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-lsp-types]
X-Description: Haskell Language Server Protocol (LSP) - data types
 An implementation of the types to allow language implementors to
 support the Language Server Protocol for their specific language.

Package: libghc-lsp-types-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-lsp-types-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-lsp-types-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
