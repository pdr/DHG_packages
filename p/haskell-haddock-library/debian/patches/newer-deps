Index: b/haddock-library.cabal
===================================================================
--- a/haddock-library.cabal
+++ b/haddock-library.cabal
@@ -1,6 +1,7 @@
 cabal-version:        3.0
 name:                 haddock-library
 version:              1.11.0
+x-revision: 5
 synopsis:             Library exposing some functionality of Haddock.
 
 description:          Haddock is a documentation-generation tool for Haskell
@@ -17,18 +18,23 @@ maintainer:           Alec Theriault <al
 homepage:             http://www.haskell.org/haddock/
 bug-reports:          https://github.com/haskell/haddock/issues
 category:             Documentation
-tested-with:          GHC == 7.4.2
-                    , GHC == 7.6.3
-                    , GHC == 7.8.4
-                    , GHC == 7.10.3
-                    , GHC == 8.0.2
-                    , GHC == 8.2.2
-                    , GHC == 8.4.4
-                    , GHC == 8.6.5
-                    , GHC == 8.8.3
-                    , GHC == 8.10.1
-                    , GHC == 9.0.1
-                    , GHC == 9.2.0
+
+tested-with:
+  GHC == 9.8.1
+  GHC == 9.6.3
+  GHC == 9.4.4
+  GHC == 9.2.7
+  GHC == 9.0.2
+  GHC == 8.10.7
+  GHC == 8.8.4
+  GHC == 8.6.5
+  GHC == 8.4.4
+  GHC == 8.2.2
+  GHC == 8.0.2
+  GHC == 7.10.3
+  GHC == 7.8.4
+  GHC == 7.6.3
+  GHC == 7.4.2
 
 extra-source-files:
   CHANGES.md
@@ -39,9 +45,9 @@ common lib-defaults
   default-language: Haskell2010
 
   build-depends:
-    , base         >= 4.5     && < 4.17
-    , containers   ^>= 0.4.2.1 || ^>= 0.5.0.0 || ^>= 0.6.0.1
-    , text         ^>= 1.2.3.0 || ^>= 2.0
+    , base         >= 4.5     && < 4.21
+    , containers   ^>= 0.4.2.1 || ^>= 0.5.0.0 || ^>= 0.6.0.1 || ^>= 0.7
+    , text         ^>= 1.2.3.0 || ^>= 2.0     || ^>= 2.1
     , parsec       ^>= 3.1.13.0
 
   ghc-options: -funbox-strict-fields -Wall
@@ -86,19 +92,19 @@ test-suite spec
     Documentation.Haddock.Parser.Identifier
 
   build-depends:
-    , base-compat  ^>= 0.12.0
-    , QuickCheck   ^>= 2.11  || ^>= 2.13.2 || ^>= 2.14 
-    , deepseq      ^>= 1.3.0.0 || ^>= 1.4.0.0
+    , base-compat  >= 0.12.0 && <0.15.0
+    , QuickCheck   >= 2.11 && <2.16
+    , deepseq      >= 1.3.0.0 && <1.6.0.0
 
   -- NB: build-depends & build-tool-depends have independent
   --     install-plans, so it's best to limit to a single major
   --     version of `hspec` & `hspec-discover` to ensure
   --     intercompatibility
   build-depends:
-    , hspec                          >= 2.4.4    && < 2.10
+    , hspec                          >= 2.4.4    && < 2.12
 
   build-tool-depends:
-    , hspec-discover:hspec-discover  >= 2.4.4    && < 2.10
+    , hspec-discover:hspec-discover  >= 2.4.4    && < 2.12
 
 test-suite fixtures
   type:             exitcode-stdio-1.0
@@ -113,11 +119,11 @@ test-suite fixtures
     , base
 
       -- extra dependencies
-    , base-compat           ^>= 0.12.0
+    , base-compat           ^>= 0.12.0 || ^>= 0.13.0
     , directory             ^>= 1.3.0.2
     , filepath              ^>= 1.4.1.2
-    , optparse-applicative  ^>= 0.15
-    , tree-diff             ^>= 0.2
+    , optparse-applicative   >= 0.15   && < 0.19
+    , tree-diff             ^>= 0.2    || ^>= 0.3
 
 source-repository head
   type:     git
