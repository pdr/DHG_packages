Source: haskell-ogma-language-smv
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Scott Talbert <swt@techie.net>
Priority: optional
Section: non-free/haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.13),
 cdbs,
 ghc,
 ghc-prof,
 alex,
 bnfc (>= 2.9.1),
 bnfc (<< 2.10),
 happy,
 libghc-quickcheck2-dev (>= 2.8.2),
 libghc-quickcheck2-dev (<< 2.16),
 libghc-quickcheck2-prof,
 libghc-test-framework-dev (>= 0.8.2),
 libghc-test-framework-dev (<< 0.9),
 libghc-test-framework-prof,
 libghc-test-framework-quickcheck2-dev (>= 0.3.0.4),
 libghc-test-framework-quickcheck2-dev (<< 0.4),
 libghc-test-framework-quickcheck2-prof,
Build-Depends-Indep: ghc-doc,
Standards-Version: 4.7.0
Homepage: https://github.com/nasa/ogma
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-ogma-language-smv
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-ogma-language-smv]
X-Description: Ogma: Runtime Monitor translator: SMV Language Frontend
 Ogma is a tool to facilitate the integration of safe runtime monitors into
 other systems. Ogma extends
 <https://github.com/Copilot-Language/copilot Copilot>, a high-level runtime
 verification framework that generates hard real-time C99 code.
 .
 This library contains a frontend to read SMV Boolean expressions, used by
 the tool FRET to capture requirement specifications.

Package: libghc-ogma-language-smv-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-ogma-language-smv-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-ogma-language-smv-doc
Architecture: all
Section: non-free/doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
