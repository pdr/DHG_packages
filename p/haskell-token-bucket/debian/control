Source: haskell-token-bucket
Section: haskell
Priority: optional
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Build-Depends: cdbs,
 debhelper (>= 10),
 ghc (>= 9.6),
 ghc-prof,
 haskell-devscripts (>= 0.13)
Build-Depends-Indep: ghc-doc
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-token-bucket
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-token-bucket]
Homepage: https://github.com/hvr/token-bucket
Rules-Requires-Root: no
X-Description: Haskell implementation of leaky bucket rate limiting
 This library implements a lazy leaky token bucket rate-limiting
 algorithm in Haskell.
 .
 You can read more about this kind of algorithm at Wikipedia:
 https://en.wikipedia.org/wiki/Token_bucket

Package: libghc-token-bucket-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-token-bucket-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-token-bucket-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
